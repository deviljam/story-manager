<article class="character_sheet_2d6" id="character_<?php echo $character['Character']['id']; ?>">
	<ul class="about">
		<li>
			<label>Name</label>
			<input type="text" name="data[Character][name]" value="<?php echo $character['Character']['name']; ?>" />
		</li>			
		<li>
			<label>Profession</label>
			<input type="text" name="data[Stat][profession]" value="<?php echo $character['StatList']['profession']; ?>" />
		</li>
		<li>
			<span class="data">
				<a id="save_character" class="button" href="/characters/edit/<?php echo $character['Character']['id']; ?>">Save</a>
			</span>
		</li>
	</ul>
	
	<div id="left_col">
		<h2>Attributes</h2>
		<ul class="attributes">
			<li>
				<label>Strength</label>
				<input type="button" name="data[Stat][Strength]" value="<?php echo $character['StatList']['strength']; ?>" />
			</li>
			<li>
				<label>Fitness</label>
				<input type="button" name="data[Stat][fitness]" value="<?php echo $character['StatList']['fitness']; ?>" />
			</li>
			<li>
				<label>Balance</label>
				<input type="button" name="data[Stat][balance]" value="<?php echo $character['StatList']['balance']; ?>" />
			</li>
			<li>
				<label>Perception</label>
				<input type="button" name="data[Stat][perception]" value="<?php echo $character['StatList']['perception']; ?>" />
			</li>
			<li>
				<label>Intelligence</label>
				<input type="button" name="data[Stat][intelligence]" value="<?php echo $character['StatList']['intelligence']; ?>" />
			</li>
			<li>
				<label>Willpower</label>
				<input type="button" name="data[Stat][willpower]" value="<?php echo $character['StatList']['willpower']; ?>" />
			</li>
		</ul>
		
		<h2>Talents</h2>
		<ul class="talents">
			<?php foreach( $character['StatList'] as $name=>$stat ) { ?>
				<?php if( preg_match('/^talent_.*$/', $name ) ) { ?>
			<li>
				<label><?php echo substr( $name, 7 ); ?></label>
				<input type="button" name="data[Stat][<?php echo $name; ?>]" value="<?php echo $stat; ?>" />
			</li>
				<?php } ?>
			<?php } ?>
			<li class="add">
				<input id="new_talent" type="input" />
				<a href="#" id="add_new_talent" data-type="talent">Add</a>
			</li>
		</ul>
		
	</div>
	
	<div id="right_col">
		<h2>Skills</h2>
		<ul class="skills">
			<?php foreach( $character['StatList'] as $name=>$stat ) { ?>
				<?php if( preg_match('/^skill_.*$/', $name ) ) { ?>
			<li>
				<label><?php echo substr( $name, 6 ); ?></label>
				<input type="button" name="data[Stat][<?php echo $name; ?>]" value="<?php echo $stat; ?>" />
			</li>
				<?php } ?>
			<?php } ?>
			<li class="add">
				<input id="new_skill" type="input" />
				<a href="#" id="add_new_skill" data-type="skill">Add</a>
			</li>
		</ul>
	</div>
	<div class="clearfix"></div>
</article>

<script src="/js/2d6_character.js"></script>
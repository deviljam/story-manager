<h2>Characters Sheets</h2>

<a class="button" href="/characters/add">Add Vampire</a>
<a class="button" href="/characters/add/2d6">Add 2D6</a>

<ul class="index">
<?php foreach ( $characters as $character ) { ?>
<li>
	<a href="/characters/view/<?php echo $character['Character']['id']; ?>">
		<?php echo $character['Character']['name']; ?>
	</a> - 
	( <?php echo $character['Character']['layout']; ?> )
</li>
<?php } ?>
</ul>
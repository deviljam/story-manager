<article class="character_sheet" id="character_<?php echo $character['Character']['id']; ?>">

	<div class="about">
		<ul>
			<li>
				<label>Name</label>
					<input type="text" name="data[Character][name]" value="<?php echo $character['Character']['name']; ?>" />
			</li>
			
		</ul>
		<ul>
			<li>
				<label>Clan</label>
				<input type="text" name="data[Character][clan]" value="<?php echo $character['Character']['clan']; ?>" />
			</li>
		</ul>
		<ul>
			<li>
				<span class="data">
					<a class="save_character" href="/characters/edit/<?php echo $character['Character']['id']; ?>">Save</a>
				</span>
			</li>
		</ul>
		<div class="clearfix"></div>
	</div>
	
	<div id="attributes">
		<ul class="physical">
			<li>
				<label>Strenght</label>
				<input type="button" name="data[Character][strength]" value="<?php echo $character['Character']['strength']; ?>" />
			</li>
			<li>
				<label>Dexterity</label>
				<input type="button" name="data[Character][dexterity]" value="<?php echo $character['Character']['dexterity']; ?>" />
			</li>
			<li>
				<label>Stamina</label>
				<input type="button" name="data[Character][stamina]" value="<?php echo $character['Character']['stamina']; ?>" />
			</li>
		</ul>
		
		<ul class="social">
			<li>
				<label>Charisma</label>
				<input type="button" name="data[Character][charisma]" value="<?php echo $character['Character']['charisma']; ?>" />
			</li>
			<li>
				<label>Manipulation</label>
				<input type="button" name="data[Character][manipulation]" value="<?php echo $character['Character']['manipulation']; ?>" />
			</li>
			<li>
				<label>Appearance</label>
				<input type="button" name="data[Character][appearance]" value="<?php echo $character['Character']['appearance']; ?>" />
			</li>
		</ul>
		
		<ul class="mental">
			<li>
				<label>Perception</label>
				<input type="button" name="data[Character][perception]" value="<?php echo $character['Character']['perception']; ?>" />
			</li>
			<li>
				<label>Intelligence</label>
				<input type="button" name="data[Character][intelligence]" value="<?php echo $character['Character']['intelligence']; ?>" />
			</li>
			<li>
				<label>Wits</label>
				<input type="button" name="data[Character][wits]" value="<?php echo $character['Character']['wits']; ?>" />
			</li>
		</ul>	
		<div class="clearfix"></div>
	</div>
	
	<div id="abilities">
		<ul class="talents">
			<li>
				<label>Alertness</label>
				<input type="button" name="data[Character][alertness]" value="<?php echo $character['Character']['alertness']; ?>" />
			</li>
			<li>
				<label>Athletics</label>
				<input type="button" name="data[Character][athletics]" value="<?php echo $character['Character']['athletics']; ?>" />
			</li>
			<li>
				<label>Brawl</label>
				<input type="button" name="data[Character][brawl]" value="<?php echo $character['Character']['brawl']; ?>" />
			</li>
			<li>
				<label>Dodge</label>
				<input type="button" name="data[Character][dodge]" value="<?php echo $character['Character']['dodge']; ?>" />
			</li>
			<li>
				<label>Empathy</label>
				<input type="button" name="data[Character][empathy]" value="<?php echo $character['Character']['empathy']; ?>" />
			</li>
			<li>
				<label>Expression</label>
				<input type="button" name="data[Character][expression]" value="<?php echo $character['Character']['expression']; ?>" />
			</li>
			<li>
				<label>Intimidation</label>
				<input type="button" name="data[Character][intimidation]" value="<?php echo $character['Character']['intimidation']; ?>" />
			</li>
			<li>
				<label>Leadership</label>
				<input type="button" name="data[Character][leadership]" value="<?php echo $character['Character']['leadership']; ?>" />
			</li>
			<li>
				<label>Streetwise</label>
				<input type="button" name="data[Character][streetwise]" value="<?php echo $character['Character']['streetwise']; ?>" />
			</li>
			<li>
				<label>Subterfuge</label>
				<input type="button" name="data[Character][subterfuge]" value="<?php echo $character['Character']['subterfuge']; ?>" />
			</li>
		</ul>
		
		<ul class="skills">
			<li>
				<label>Animal Ken</label>
				<input type="button" name="data[Character][animal_ken]" value="<?php echo $character['Character']['animal_ken']; ?>" />
			</li>
			<li>
				<label>Craft</label>
				<input type="button" name="data[Character][crafts]" value="<?php echo $character['Character']['crafts']; ?>" />
			</li>
			<li>
				<label>Drive</label>
				<input type="button" name="data[Character][drive]" value="<?php echo $character['Character']['drive']; ?>" />
			</li>
			<li>
				<label>Etiquette</label>
				<input type="button" name="data[Character][etiquette]" value="<?php echo $character['Character']['etiquette']; ?>" />
			</li>
			<li>
				<label>Firearms</label>
				<input type="button" name="data[Character][firearms]" value="<?php echo $character['Character']['firearms']; ?>" />
			</li>
			<li>
				<label>Melee</label>
				<input type="button" name="data[Character][melee]" value="<?php echo $character['Character']['melee']; ?>" />
			</li>
			<li>
				<label>Performance</label>
				<input type="button" name="data[Character][performance]" value="<?php echo $character['Character']['performance']; ?>" />
			</li>
			<li>
				<label>Security</label>
				<input type="button" name="data[Character][security]" value="<?php echo $character['Character']['security']; ?>" />
			</li>
			<li>
				<label>Stealth</label>
				<input type="button" name="data[Character][stealth]" value="<?php echo $character['Character']['stealth']; ?>" />
			</li>
			<li>
				<label>Survival</label>
				<input type="button" name="data[Character][acedemics]" value="<?php echo $character['Character']['acedemics']; ?>" />
			</li>
		</ul>
		
		<ul class="knowledges">
			<li>
				<label>Acedemics</label>
				<input type="button" name="data[Character][acedemics]" value="<?php echo $character['Character']['acedemics']; ?>" />
			</li>
			<li>
				<label>Computer</label>
				<input type="button" name="data[Character][computer]" value="<?php echo $character['Character']['computer']; ?>" />
			</li>
			<li>
				<label>Finance</label>
				<input type="button" name="data[Character][finance]" value="<?php echo $character['Character']['finance']; ?>" />
			</li>
			<li>
				<label>Investigation</label>
				<input type="button" name="data[Character][investigation]" value="<?php echo $character['Character']['investigation']; ?>" />
			</li>
			<li>
				<label>Law</label>
				<input type="button" name="data[Character][empathy]" value="<?php echo $character['Character']['empathy']; ?>" />
			</li>
			<li>
				<label>Linguistics</label>
				<input type="button" name="data[Character][linguistics]" value="<?php echo $character['Character']['linguistics']; ?>" />
			</li>
			<li>
				<label>Medicine</label>
				<input type="button" name="data[Character][medicine]" value="<?php echo $character['Character']['medicine']; ?>" />
			</li>
			<li>
				<label>Occult</label>
				<input type="button" name="data[Character][occult]" value="<?php echo $character['Character']['occult']; ?>" />
			</li>
			<li>
				<label>Politics</label>
				<input type="button" name="data[Character][politics]" value="<?php echo $character['Character']['politics']; ?>" />
			</li>
			<li>
				<label>Science</label>
				<input type="button" name="data[Character][science]" value="<?php echo $character['Character']['science']; ?>" />
			</li>
		</ul>
		<div class="clearfix"></div>
	</div>
	
	<div class="advantages">
		<ul class="equipment">
			<?php foreach ( $character['CharacterEquipment'] as $equipment ) { ?>
			<li>
				<select name="data[CharacterEquipment][<?php echo $equipment['id']; ?>][prop_id]">
					<?php foreach ( $equipment_list as $key=>$eqp ) { ?>
						<option value="<?php echo $key; ?>"<?php echo $equipment['prop_id'] == $key ? ' selected="selected" ' : ''; ?>><?php echo $eqp; ?></option>
					<?php } ?>										
				</select>
			</li>
			<?php } ?>
			<li>
				<a href="/characters/xhr_newprop/5/<?php echo $character['Character']['id']; ?>" class="addprop">Add Equipment</a>
			</li>
		</ul>
		<ul class="disciplines">
			<?php foreach ( $character['CharacterDiscipline'] as $discipline ) { ?>
			<li>
				<select name="data[CharacterDiscipline][<?php echo $discipline['id']; ?>][prop_id]">
					<?php foreach ( $discipline_list as $key=>$dis ) { ?>
						<option value="<?php echo $key; ?>"<?php echo $discipline['prop_id'] == $key ? ' selected="selected" ' : ''; ?>><?php echo $dis; ?></option>
					<?php } ?>										
				</select>
				<input type="button" name="data[CharacterDiscipline][<?php echo $discipline['id']; ?>][level]" value="<?php echo $discipline['level']; ?>" />
			</li>
			<?php } ?>
			<li>
				<a href="/characters/xhr_newprop/4/<?php echo $character['Character']['id']; ?>" class="addprop">Add Discipline</a>
			</li>
		</ul>
		<ul class="discipline">
			
		</ul>
		<div class="clearfix"></div>
	</div>
</article>

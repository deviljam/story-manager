<h2>
	Props
	<a href="/props/add">Add</a>
</h2>

<ul class="linear">
	<?php foreach ( $categories as $key => $category ) { ?>
	<li>
		<a href="/props/index/<?php echo $key; ?>"><?php echo $category; ?></a>
	</li>
	<?php } ?>
</ul>

<div class="colm">
	<ul class="left_colm index">
		<?php 
		foreach ( $props as $prop ) { 
			if ( $prop['Prop']['category_id'] > 0 ) {
		?>
		<li>
			<a class="prop_article" href="/props/view/<?php echo $prop['Prop']['slug']; ?>">
				<?php echo $prop['Prop']['name']; ?>
			</a>
			 - ( <?php echo $categories[ $prop['Prop']['category_id'] ]; ?> )
		</li>
		<?php } } ?>
	</ul>
	<div class="right_colm" id="prop_article">
	</div>
</div>

<script type="text/javascript">
$('.prop_article').click(function(e){
	e.preventDefault();

	$.ajax( {
		url : this.href + '/layout:ajax',
		success : function(res) {
			var element = document.getElementById('prop_article');
			element.innerHTML = res;
		}
	});
});
</script>
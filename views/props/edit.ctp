<?php

	echo $this->Form->create ( 'Prop' );
	echo $this->Form->input ( 'name' );
	echo $this->Form->input ( 'category_id', array( 'options' => $categories ) );
	echo $this->Form->input ( 'public', array( 'options' => array( 0 => 'Private', 1 => 'Everyone') ) );
	echo $this->Form->input ( 'description' );
	echo $this->Form->input ( 'lat', array( 'label' => 'Latitude' ) );
	echo $this->Form->input ( 'lng', array( 'label' => 'Longitude' ) );
	echo $this->Form->submit();
?>
<div id="googlemap" ></div>

<a href="/props/add">Add</a>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script type="text/javascript">
	var g_map_elm = document.getElementById( 'googlemap' );
	var g_map = false;
	g_map_elm.style.width = "100%";
	g_map_elm.style.height = "800px";
	
	var prop_data = <?php echo json_encode ( $props ); ?>;
	
	function markerCallWindow() {
		var url = "/props/view/";
		url += this.prop_data.Prop.id;
		showPropertyWindow ( url );
	}
	
	var gOptions = {
		zoom: 14,
		mapTypeId: google.maps.MapTypeId.SATELLITE
	};
	
	g_map = new google.maps.Map( g_map_elm, gOptions );
	g_map.setCenter ( new google.maps.LatLng( 40.446947,-79.993515 ) );
	
	for ( var i in prop_data ) {
		var latlng = new google.maps.LatLng( prop_data[i].Prop.lat, prop_data[i].Prop.lng );
		var marker = new google.maps.Marker( { 
			position: latlng,
			title : prop_data[i].Prop.name
		} );
		marker.prop_data = prop_data[i];
		google.maps.event.addListener( marker, 'click', markerCallWindow );
		marker.setMap ( g_map );
	}
</script>
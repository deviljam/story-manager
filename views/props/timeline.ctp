<ul class="timeline">
	<?php foreach ( $props as $prop ) { ?>
		<li>
			<label>
				<?php echo date ( "Y: l dS F", strtotime( $prop['Time']['date'] ) ); ?>
			</label>
			<a class="slug" href="/props/view/<?php echo $prop['Prop']['slug']; ?>">
				<?php echo $prop['Prop']['name']; ?>
			</a>
			<p>
				<?php echo $prop['Time']['sentence']; ?>
			</p>
		</li>
	<?php } ?>
</ul>
<?php

class SluggerHelper extends AppHelper {

	function apply( $str ) {
		//Apply all formatting
		$str = $this->format ( $str );
		$str = $this->slug ( $str );
		return $str;
	}
	
	function format ( $str ) {
		$str = preg_replace ( '/\[(.+?)\|(.+?)\]/', '<div class="columns_indent"><div class="column_small">${1}</div><div class="column_large">${2}</div></div>', $str ); //indented columns
		
		$str = preg_replace ( '/\(skill:(\d+)\)+/', '<img src="\img\level_dots_$1.png" alt="skill $1"/>', $str ); //skill bars
		$str = preg_replace ( '/(\n|\r)+/', '</p><p>', $str ); //line breaks
		$str = preg_replace ( '/\*(.+?)\*/', '<b>${1}</b>', $str ); //bold
		
		$str = preg_replace ( '/\[\[\[(.+?)\]\]\]/', '<h4>${1}</h4>', $str ); //label
		$str = preg_replace ( '/\[\[(.+?)\]\]/', '<h3>${1}</h3>', $str ); //label
		$str = preg_replace ( '/\[(.+?)\]/', '<h2>${1}</h2>', $str ); //label
		return $str;
	}
	
	function slug ( $str ) {
		//Add slugs 
		$str = preg_replace ( '/@([a-zA-Z0-9_]+)/', '<a class="slug" href="/props/view/${1}">${0}</a>', $str );				$str = preg_replace ( '/#([a-zA-Z0-9_]+)/', '<a class="slug" href="/paragraphs/summary/${1}">${0}</a>', $str );
		return $str;
	}

}

?>
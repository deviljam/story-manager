You have been invite to play <?php echo $name; ?> on the following date:

<?php echo date( "l, jS F Y", strtotime( $date ) ); ?>

<?php echo $notes; ?>

To confirm your attendance click the link below.

http://story.birdynest.com/schedules/approve/<?php echo $schedule_id; ?>/1/<?php echo $hash; ?>


However, if you're unable to make it, click below.

http://story.birdynest.com/schedules/approve/<?php echo $schedule_id; ?>/-1/<?php echo $hash; ?>


Thanks for you time! Hope to hear from you soon!

-James
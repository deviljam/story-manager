<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		Story Manager | <?php echo $title_for_layout; ?>
	</title>
	
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('style');
	?>
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/script.js"></script>
	<script type="text/javascript" src="/js/property_window.js"></script>
	<script type="text/javascript" src="/js/character.js"></script>
	<script type="text/javascript" src="/js/paragraphs.js"></script>
</head>
<body>
	<div id="container">
		<header>
			<h1>Prototype</h1>
			<nav>
				<a href="/">Home</a>
				<?php if ( $loggedin ) { ?>
					<a href="/stories">Stories</a>
				<?php } ?>
				<a href="/props/">Props</a>
				<?php if ( $loggedin ) { ?>
					<a href="/characters/">Characters</a>
				<?php } ?>
				<?php if ( $loggedin ) { ?>
					<a href="/paragraphs">Paragraphs</a>
				<?php } ?>
				<a href="/props/map">Map</a>
				
				<?php if ( $this->Session->check( 'Auth.User.id' ) ) { ?>
					<a class="right" href="/users/logout">Logout</a>
				<?php } else  { ?>
					<a class="right" href="/users/login">Login</a>
				<?php } ?>
				
			</nav>		

		</header>
		
		<div id="content">

			<?php echo $this->Session->flash(); ?>
			<?php echo $this->Session->flash('email'); ?>
			
			<section id="properties">
				<a id="properties_close" class="close">Close</a>
				<div id="properties_data">
					<!-- ajax data fills this space -->
				</div>
			</section>

			<?php echo $content_for_layout; ?>

		</div>
		<footer>

		</footer>
	</div>
</body>
</html>
<?php 

	echo $this->Form->create ( 'Chapter' );
	echo $this->Form->input( 'name' );
	echo $this->Form->input( 'story_id', array ( 'options' => $story_options ) );
	echo $this->Form->input( 'prop_id', array ( 'options' => $props_options ) );
	echo $this->Form->input( 'body' );
	
	echo $this->Form->submit();
	
?>
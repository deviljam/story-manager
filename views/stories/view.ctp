<h2>
	<?php echo $story['Story']['name']; ?>
	<a href="/chapters/add/<?php echo $story_id; ?>">Add</a>
</h2>
<!-- Campaign Map -->


<div id="storymap" >
<?php if ( !empty ( $story['Chapters'] ) ) { ?>
	<?php foreach ( $story['Chapters'] as $chapter ) { ?>
	<div 
		class="chapter<?php echo $chapter['complete']['complete'] > 0 ? ' complete' : ''; ?>"
		style="top:<?php echo $chapter['pos_y']; ?>px; left:<?php echo $chapter['pos_x']; ?>px;" 
		id="chapter_<?php echo $chapter['id']; ?>"
	>
		<div class="dragarea">
		</div>
		<p>
			<?php echo $chapter['name']; ?>
		</p>
		<a class="more" href="/chapters/view/<?php echo $chapter['id']; ?>" >
			More
		</a>
		<a class="complete" href="/chapters/complete/<?php echo $chapter['id']; ?>">Complete</a>
	</div>
	<?php } ?>
<?php } ?>
</div>

<script type="text/javascript" src="/js/storymap.js"></script>
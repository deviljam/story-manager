<h2>
	Players
	<a href="/players/add">Add</a>
</h2>

<?php if ( !empty ( $players ) ) { ?>
<ul class="index">
	<?php foreach ( $players as $player ) { ?>
	<li>
		<?php echo $player['Player']['name']; ?>
		-
		<a href="/players/edit/<?php echo $player['Player']['id']; ?>" >
			Edit
		</a>
		-
		<a href="/players/delete/<?php echo $player['Player']['id']; ?>" >
			Delete
		</a>
	</li>
	<?php } ?>
</ul>
<?php } ?>

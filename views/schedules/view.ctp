<div class="session">
	<h3><?php echo date('dS F Y', strtotime( $event['Schedule']['date'] ) ); ?></h3>
	
	<ul class="players">
	<?php foreach ( $event['SchedulesPlayer'] as $player ) { ?>
		<?php if ( !empty( $player['Player'] ) ) { ?>
		<?php 
			switch ( $player['attending'] ) {
				case 1: $class = 'attending'; break;
				case -1: $class = 'notattending'; break;
				default: $class = 'pending'; break;
			}				
		?>
		<li>
			<?php echo $player['Player']['name']; ?>
			<div class="<?php echo $class; ?>"></div>
		</li>
		<?php } ?>
	<?php } ?>
	</ul>
</div>
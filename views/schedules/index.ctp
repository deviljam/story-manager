<h2>
	Schedule
	<?php if ( $this->Session->check( 'Auth.User.id' ) ) { ?>
	<a href="/schedules/add">Add</a>
	<?php } ?>
</h2>

<div id="calendar">
<?php foreach ( $events as $event ) { ?>
	<div title="<?php echo $event['Schedule']['date']; ?>">
		<a class="slug" href="/schedules/view/<?php echo $event['Schedule']['id']; ?>"><?php echo $event['Schedule']['name']; ?></a>
	</div>
	
<?php } ?>
</div>


<div class="clearfix"></div>

<script src="/js/calendar.js" type="text/javascript"></script>
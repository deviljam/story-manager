<h2>
	Paragraphs
	<a href="#" id="add_new">Add</a>
	<a href="/paragraphs/all">View All</a>
</h2>

<ul class="index">
	<?php
		if ( !empty ( $articles ) ) { 
			foreach ( $articles as $key=>$article ) { 
	?>
	<li>
		<a href="/paragraphs/summary/<?php echo $key;?>">
			<?php echo "$key"; ?>
		</a>
		- ( <?php echo $article['count']; ?> ) 
	</li>
	<?php } } ?>
</ul>

<script>
	onload_buffer.push ( function() {
		document.getElementById( 'add_new' ).onclick = function ( ev ) {
			$.ajax ( {
				url : 'paragraphs/add',
				data : { data : { Paragraph : { body : "#blank_article" } } },
				type : 'POST',
				success : function( response ) {
					res = JSON.parse( response );
					
					window.location = "/paragraphs/summary/blank_article" ;
				}
			} );
			ev.preventDefault();
		}
	} );
</script>
<h2>
	<?php echo $filter; ?>
	<a href="#" id="add_new">Add</a>
</h2>

<ul id="para_list">
	<?php
		if ( !empty ( $paragraphs ) ) { 
			foreach ( $paragraphs as $para ) { 
	?>
	<li class="paragraph" data-id="<?php echo $para['Paragraph']['id']; ?>">
		<?php echo $para['Paragraph']['body']; ?>
	</li>
	<?php } } ?>
</ul>

<script>
	var default_tag = "<?php echo $filter; ?>";
	function submit_filter ( ) {
		filter_value = document.getElementById( 'filter_search' ).value;
		paraList = document.getElementById ( 'para_list' );
		paraList.innerHTML = "";
		
		$.ajax ( {
			url : 'paragraphs/json_filter/' + filter_value,
			success : function( response ) {
				res = JSON.parse( response );
				paraList = document.getElementById ( 'para_list' );
				paraList.innerHTML = "";
				
				for ( var i = 0; i < res.length; i++ ) {
					temp = document.createElement( 'li' );
					temp.className = "paragraph";
					temp.setAttribute('data-id', res[i].Paragraph.id );
					temp.innerHTML = res[i].Paragraph.body;
					paraList.appendChild( temp );
					new Paragraph ( temp );		
				}
			}
		} );
	}
	
	
	function apply_hash( hash ) { 
		window.location = "/paragraphs/summary/" + hash;
		return false;
	}
	
	onload_buffer.push ( function() {
		document.getElementById( 'add_new' ).onclick = function ( ev ) {
			var default_body = "";
			if ( default_tag != undefined ) { default_body = "#" + default_tag}
			$.ajax ( {
				url : '/paragraphs/add',
				data : { data : { Paragraph : { body : default_body } } },
				type : 'POST',
				success : function( response ) {
					res = JSON.parse( response );
					
					paraList = document.getElementById ( 'para_list' );
					
					temp = document.createElement( 'li' );
					temp.className = "paragraph";
					temp.setAttribute('data-id', res.Paragraph.id );
					temp.innerHTML = res.Paragraph.body;
					
					$( paraList ).prepend( temp );
					new Paragraph ( temp );
					
				}
			} );
			ev.preventDefault();
		}
		/*
		document.getElementById( 'filter' ).onclick = function( ev ) { 
			submit_filter()
			ev.preventDefault();
		}
		
		document.getElementById( 'clear' ).onclick = function( ev ) { 
			document.getElementById( 'filter_search' ).value = "";
			submit_filter();
			ev.preventDefault();
		}
		*/

	} );
</script>
<style>
.character_template{
	padding:10px;
	border:1px solid #333;
	position:relative;
	min-height:170px;
	margin-bottom:20px;
}
.character_template input[name=name]{
	position:absolute;
	left:170px;
	right:60px;
}
.character_template input[name=fitness]{
	position:absolute;
	right:10px;
	width:40px;
}
.body_diagram {
	position:absolute;
	width:150px;
	height:125px;
	border:1px solid #AAA;
	left:10px;
	top:10px;
}
.body_diagram li {
	position:absolute;
	background:green;
	width:30px;
	height:30px;
	border:2px solid #DDD;
	cursor:pointer;
	list-style:none;
}
.body_diagram .head{
	top:5px;
	left:55px;
}
.body_diagram .body{
	top:45px;
	left:55px;
}
.body_diagram .arm_left{
	top:45px;
	left:95px;
}
.body_diagram .arm_right{
	top:45px;
	left:15px;
}
.body_diagram .leg_left{
	top:85px;
	left:75px;
}
.body_diagram .leg_right{
	top:85px;
	left:35px;
}
.body_diagram li.active{
	border:2px solid #FA0;
}
.hitselect {
	width:150px;
	height:10px;
	position:absolute;
	left:10px;
	top:145px;
	list-style:none;
}
.levelselect {
	width:150px;
	height:10px;
	position:absolute;
	left:10px;
	top:165px;
	list-style:none;
}
.hitselect li, .levelselect li {
	float:left;
	width:15px;
	height:15px;
	margin-right:10px;
	background:green;
	border-radius:3px;
	cursor:pointer;
	box-shadow:0 2px 0 RGBA(0,0,0,0.2);
}
.hitselect li.active, .levelselect li.active {
	box-shadow:0 2px 0 RGBA(0,0,0,0.5) inset;
}
.status {
	border:1px solid #AAA;
	position:relative;
	min-height:20px;
	margin:30px 0 10px 160px;
	list-style:none;
}
.status li {
	padding:5px 10px; 
	border-bottom:1px solid #AAA;
}
.status li:last-child {
	border:none
}
.status a.delete {
	float:right;
	background:#A00;
	width:15px;
	height:15px;
	border-radius:3px;
	cursor:pointer;
	text-align:center;
	color:white;
}
.roll_damage{
	padding:3px 5px;
	margin:0 0 0 160px;
}

</style>

<div id="characters"></div>

<input type="button" id="add_character" value="add Character" />

<script type="text/javascript">
function Character(parent){
	this.parent = parent;
	this.createElement();
}
Character.prototype.createElement = function(){
	this.types = [
		'bludgeoning',
		'stabbing',
		'slashing',
		'sanity',
		'fire',
		'toxic'
	];
	this.element = document.createElement('div');
	
	var body = document.createElement('ul');
	this.body = {
		head : document.createElement('li'),
		body : document.createElement('li'),
		arm_right : document.createElement('li'),
		arm_left : document.createElement('li'),
		leg_right : document.createElement('li'),
		leg_left : document.createElement('li')
	}
	this.name = document.createElement('input');
	this.current_fitness = document.createElement('input');
	this.fitness = document.createElement('input');
	
	var hitselect = document.createElement('ul');
	this.hitlist = {
		bludgeoning : document.createElement('li'),
		stabbing : document.createElement('li'),
		slashing : document.createElement('li'),
		sanity : document.createElement('li'),
		fire : document.createElement('li'),
		toxic : document.createElement('li')
	}
	
	var levelselect = document.createElement('ul');
	this.levellist = {
		l1 : document.createElement('li'),
		l2 : document.createElement('li'),
		l3 : document.createElement('li'),
		l4 : document.createElement('li'),
		l5 : document.createElement('li'),
		l6 : document.createElement('li')
	}
	
	this.status = document.createElement('ul');
	this.roll = document.createElement('input');
	
	//Add classes
	this.element.className = "character_template";
	body.className = "body_diagram";
	this.name.name = "name";
	this.fitness.name = "fitness";
	hitselect.className = "hitselect";
	levelselect.className = "levelselect";
	this.status.className = "status";
	this.body.head.className = "head";
	this.body.body.className = "body";
	this.body.arm_right.className = "arm_right";
	this.body.arm_left.className = "arm_left";
	this.body.leg_right.className = "leg_right";
	this.body.leg_left.className = "leg_left";
	this.roll.className = "roll_damage";
	this.roll.type = "button";
	this.roll.value = "Roll Damage";
	
	//Merge
	body.appendChild(this.body.head);
	body.appendChild(this.body.body);
	body.appendChild(this.body.arm_right);
	body.appendChild(this.body.arm_left);
	body.appendChild(this.body.leg_right);
	body.appendChild(this.body.leg_left);
	hitselect.appendChild(this.hitlist.bludgeoning);
	hitselect.appendChild(this.hitlist.stabbing);
	hitselect.appendChild(this.hitlist.slashing);
	hitselect.appendChild(this.hitlist.sanity);
	hitselect.appendChild(this.hitlist.fire);
	hitselect.appendChild(this.hitlist.toxic);
	levelselect.appendChild(this.levellist.l1);
	levelselect.appendChild(this.levellist.l2);
	levelselect.appendChild(this.levellist.l3);
	levelselect.appendChild(this.levellist.l4);
	levelselect.appendChild(this.levellist.l5);
	levelselect.appendChild(this.levellist.l6);
	
	
	this.element.appendChild(body);
	this.element.appendChild(this.name);
	this.element.appendChild(this.fitness);
	this.element.appendChild(hitselect);
	this.element.appendChild(this.status);
	this.element.appendChild(this.roll);
	this.element.appendChild(levelselect);
	
	this.parent.appendChild( this.element );
	
	//Add listeners
	var self = this;
	$([hitselect,levelselect,body]).find('li').click(function(e){
		$(this).parent().find('li').removeClass('active');
		$(this).addClass('active');
	});
	$(this.roll).click(function(e){
		self.roll_damage();
	});
}
Character.prototype.addStatus = function(str){
	var li = document.createElement('li');
	li.innerHTML = str + "<a class='delete'>X</a>";
	this.status.appendChild(li);
}
Character.prototype.roll_damage = function(){
	
	this.types[ type ];
};

$(document).ready(function(){
	window.parent = document.getElementById('characters');
	window.characters = [];
	
	document.getElementById('add_character').onclick = function(){
		window.characters.push( new Character( window.parent ) ); 
	}
});

var Damage ={
	data : <?php echo json_encode($damage); ?>,
	
	getStatus : function(type,level,chp){
		try{
			var dam_list = this.data[ type ][ level ];
			dam_list.sort(function(a,b){return Math.random()-0.5;});
			
			for(var i = 0; i < dam_list.length; i++ ) {
				if( chp >= dam_list[i].min_damage ) {
					return dam_list[i];
				}
			}
		} catch( err ) {
			return false;
		}
	}
};
</script>
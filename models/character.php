<?php

class Character extends AppModel {

	var $order = "Character.name";
	
	var $hasMany = array(
		'CharacterEquipment',
		'CharacterDiscipline',
		'CharacterStat' => array(
			'order' => 'name'
		)
	);
	
	function save( $data ) {
		if ( !empty ( $data['CharacterEquipment'] ) ) foreach ( $data['CharacterEquipment'] as $key => $dis ) {
			$temp = array ( 'CharacterEquipment' => $dis );
			if ( $key == 'new' ){
				$this->CharacterEquipment->id = $key;
			} else {
				$temp['CharacterEquipment']['character_id'] = $this->id;
			}
			$this->CharacterEquipment->save ( $temp );
		}
		
		if ( !empty ( $data['CharacterDiscipline'] ) ) foreach ( $data['CharacterDiscipline'] as $key => $dis ) {
			$temp = array ( 'CharacterDiscipline' => $dis );
			if ( $key != 'new' ){
				$this->CharacterDiscipline->id = $key;
			} else {
				$temp['CharacterDiscipline']['character_id'] = $this->id;
			}
			$this->CharacterDiscipline->save ( $temp );
		}
		
		if ( !empty ( $data['Stat'] ) ) foreach ( $data['Stat'] as $key => $value ) {
			$stats = false;
			$stats = $this->CharacterStat->find('all', array( 'conditions' => array( 'name' => $key, 'character_id' => $this->id ) ) );
			//echo json_encode ( $stats );
			if ( empty ( $stats ) ){
				$stat = array( 'CharacterStat' => array( 'name' => $key, "character_id" => $this->id ) );
				$this->CharacterStat->create();
			} else { 
				$stat = $stats[0];
			}
			$stat['CharacterStat']['value'] = $value;
			$this->CharacterStat->save( $stat );
		}
		
		return parent::save( $data );
	
	}
}

?>
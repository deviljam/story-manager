<?php

class Prop extends AppModel {
	
	var $order = 'Prop.name';
	
	var $belongsTo = array (
		'Category'
	);
	
	var $hasMany = array(
		'CharacterDiscipline'
	);

	function findSlug ( $slug ) {
		//Find or create missing prop
		$prop = $this->find( 'first', array( 'conditions' => array( 'slug' => $slug ) ) );
		
		if ( empty ( $prop ) ) {
		
			$prop = array ( 'Prop' => array (
				'name' => 'This prop does not yet exist',
				'slug' => $slug,
				'description' => ''
			) );
				
			$this->save ( $prop );
			
			$prop['Prop']['id'] = $this->getInsertID();
		}
		
		return $prop;
	}
	
	function toSlug ( $slug ) {
		$slug = strtolower ( $slug );
		$slug = preg_replace ( '/[^a-z0-9\s]*/', '', $slug );
		$slug = preg_replace ( '/\s+/', '_', $slug );
		
		return $slug;
	}
	
	function save ( $data ) {
		//Override the save function to automatically create a slug
		if ( !isset( $data['Prop']['slug'] ) && $this->id < 1 ) {
			$data['Prop']['slug'] = $this->toSlug( $data['Prop']['name'] );
		} 
		return parent::save ( $data );
			
	}
	
	function timeline( $params = false ) {
		$props = $this->find ('all', $params);
		$out = array();
		$count = 0;
		
		$sentence_break = "/$|\n|\r|\./";
		$date_regex = "/\((?'date'(?'year'\d\d\d\d)-(?'month'\d\d)-(?'day'\d\d)(\s+(?'hour'\d\d):(?'minute'\d\d))?)\)/";
		
		//Search throug all props looking for timestamps
		foreach ( $props as $prop ) {
			$sentences = preg_split ( $sentence_break, $prop['Prop']['description'] );
			
			foreach ( $sentences as $sentence ) {
			
				if ( preg_match_all ( $date_regex, $sentence, $match ) ) {
					foreach ( $match[1] as $index=>$date ) {
						$out[ $count ] = $prop;
						$out[ $count ]['Time'] = array (
							'sentence' => $sentence,
							'date' => $match['date'][$index],
							'year' => $match['year'][$index],
							'month' => $match['month'][$index],
							'day' => $match['day'][$index],
							
							'hour' => $match['hour'][$index],
							'minute' => $match['minute'][$index]
						);
						$out[ $count ]['Time']['hour'] = $out[ $count ]['Time']['hour'] ? $out[ $count ]['Time']['hour'] : '12'; 
						$out[ $count ]['Time']['minute'] = $out[ $count ]['Time']['minute'] ? $out[ $count ]['Time']['minute'] : '00'; 
						
						//Set Time Stamp
						$stamp = $out[ $count ]['Time'];
						$out[ $count ]['Time']['stamp'] = strtotime ( 
							$stamp['year'] ."-". $stamp['month'] ."-". $stamp['day'] ." ". $stamp['hour'] .":". $stamp['minute'] .":00"
						);
						$count++;
					}
				}
			}
		}
		
		usort ( $out, "timesort" );
		return $out;
	}
	
}

?>
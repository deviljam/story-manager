<?php

class Paragraph extends AppModel {

	var $order = array ( 'eventtime DESC' );

	function save ( $data ) {
		// Find Time stamps in body, and set the eventtime field based on the latest 
		
		if ( isset ( $data['Paragraph']['body'] ) ) {
			//Find Time
			$time = strtotime( "0000-00-00" );
			$matched = preg_match_all ( '/\((\d\d\d\d-\d\d-\d\d(\s\d\d:\d\d(:\d\d)?)?)\)/', $data['Paragraph']['body'], $matches );
			//pr ( $matches ); die;
			if ( $matched ) {
				//Set time to the first time
				
				$time = strtotime( $matches[1][0] );
				//Check for latest timestamp
				foreach ( $matches[1] as $match ){
					//matches[1] is group 1 of matches
					$temp = strtotime( $match );
					if ( $temp > $time ){ 
						$time = $temp;
					}
				}
			}
			//Only set the eventtime if a body was provided
			$data['Paragraph']['eventtime'] = date ( "Y-m-d H:i:s", $time );
		}
		
		return parent::save( $data );
	}
	
	function filter( $filter = false, $associates_level = 0 ){
		if ( $filter ) {
			$matches = preg_split( '/[^a-zA-Z_]/', strtolower ( $filter ) );
			
			$and_conditions = array();
			foreach ( $matches as $match ) {
				$and_conditions[] = array( "body like" => "%#$match%" );
			}
			$conditions = array ( "AND" => $and_conditions );
			
			if( $associates_level > 0 ) {
				$or_filters = $this->get_associated_filters($filter, $associates_level);
				$or_conditions = array();
				
				foreach( $or_filters as $or_filter ){ 
					$or_conditions[] = array( "body like" => "%#$or_filter%" );
				}
				$conditions = array( "OR" => $or_conditions );
			}
			
			return $this->find('all', array(
				'conditions' => $conditions)
			);
		} 
		
		return $this->find('all');
		
	}
	
	private function get_associated_filters( $filter, $associates_level = 1 ) {
		$out = array();
		
		if ( $filter ) {
			$matches = preg_split( '/[^a-zA-Z_]/', strtolower ( $filter ) );
			
			$and_conditions = array();
			foreach ( $matches as $match ) {
				$and_conditions[] = array( "body like" => "%#$match%" );
			}
			$conditions = array ( "AND" => $and_conditions );
			
			$paras = $this->find('all', array(
				'conditions' => $conditions)
			);
			
			foreach( $paras as $para ) {
				preg_match_all( '/#[a-zA-Z_]+/', strtolower( $para['Paragraph']['body'] ), $tags );
				foreach( $tags[0] as $tag ) {
					$out[] = str_replace("#","",$tag);
				}
			}
			
			if( $associates_level > 1 ) {
				$recur = $this->get_associated_filters( implode($out,","), $associates_level-1);
				$out = array_merge($out, $recur);
			}
			
			return $out;
		}
	}

}
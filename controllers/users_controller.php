<?php

class UsersController extends AppController {
	
	function login( $redirect = false ) {
	
		if ( !$redirect ) {
			$redirect = "/";
		}
		
		if ( !empty ( $this->data ) ) {
		
			if ( $this->Auth->login( $this->data ) ) {
				$this->flashSuccess( 'You have successfully logged in', $redirect );
			} else { 
				$this->flashFailure( 'Username and password not found' );
			}
			
		}
	}
	
	function logout( $redirect = false ) {
		$this->redirect($this->Auth->logout());
		$this->flashSuccess( "You have logged out", "/" );		
	}
	
	function register( $redirect = false ) {
		if ( !$redirect ) {
			$redirect = "/";
		}
		
		if ( !empty ( $this->data ) ) {
		
			if ( $this->Auth->login( $this->data ) ) {
				$this->flashSuccess( 'You have successfully logged in', $redirect );
			} else { 
				$this->flashFailure( 'Username and password not found' );
			}
			
		}
	}
	
} 

?>
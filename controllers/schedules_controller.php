<?php

class SchedulesController extends AppController {
		
	function beforeFilter() {
		parent::beforeFilter();
		
		$this->Auth->allow( 'index', 'view', 'approve' );
	}
	
	function index ( ) {
		$events = $this->Schedule->find('all' );
		$this->set( 'events', $events );
	}
	
	function view ( $id ) {
		$this->Schedule->id = $id;
		$event = $this->Schedule->read();
		
		$this->set( 'event', $event );
	}
	
	function add ( ) {	
		if ( !empty( $this->data ) ) {
			if ( $this->Schedule->save ( $this->data ) ) {
				$id = $this->Schedule->getInsertID();
				$this->_attach_players( $id );
				$this->flashSuccess( 'A new schedule has been create', '/' );
			} else {
				$this->flashFailure( 'There was a problem creating a schedule' );
			}
		}
	}
	
	function approve ( $schedule_id, $approve, $hash ) {
		$this->loadModel ( 'SchedulesPlayer' );
		
		//Match URL with hash
		$schedule = $this->SchedulesPlayer->find('first', array( 'conditions' => array ( 'schedule_id' => $schedule_id, 'hash' => $hash ) ) );
		
		if ( !empty( $schedule ) ) {
			$schedule['SchedulesPlayer']['attending'] = $approve;
			$data['SchedulesPlayer'] = $schedule['SchedulesPlayer'];
			
			if ( $this->SchedulesPlayer->save ( $data ) ) {
				//A successful update to the user's attendance has been made
				if ( $approve > 0 ) {
					$this->flashSuccess( 'Your attendance has been confirmed. Thank you.', '/' );
				} else {
					$this->flashSuccess( 'Sorry to hear that. Maybe next time.', '/' );
				}
			} else {
				$this->flashFailure( 'Unable to resolve your attendance. Please contact the administrator.', '/' );
			}
		} else { 
			$this->flashFailure( 'Unable to resolve your attendance. Please contact the administrator.', '/' );
		}
		
		$this->flashFailure( 'An unknown error occured. Please contact the administrator.', '/' );
	}
	
	
	function _attach_players ( $schedule_id ) {
		$this->loadModel ( 'Player' );
		$this->loadModel ( 'SchedulesPlayer' );
		
		$this->Schedule->id = $schedule_id;
		$schedule = $this->Schedule->read();
		$party = $schedule['Schedule']['party'];
		$conditions = array( 'Player.parties LIKE' => "%$party%" );
		$players = $this->Player->find('all', array( 'conditions' => $conditions) );
		$count = 0;
		
		foreach ( $players as $player ) {
			unset ( $data );
			$data['SchedulesPlayer']['schedule_id'] = $schedule_id;
			$data['SchedulesPlayer']['player_id'] = $player['Player']['id'];
			$data['SchedulesPlayer']['hash'] = Security::hash( $player['Player']['id'] . $schedule_id );
			$data['SchedulesPlayer']['attending'] = 0;
			
			$this->SchedulesPlayer->create();
			if ( $this->SchedulesPlayer->save ( $data ) ) {
				$this->Email->reset();
				//$this->Email->delivery = 'debug';
				$this->Email->from = 'storybot@birdynest.com';
				$this->Email->to = $player['Player']['email'];
				$this->Email->subject = 'Table Top RPGS: Ready for ' . $schedule['Schedule']['name'];
				$this->Email->template = 'approve';
				
				$this->set( 'name', $schedule['Schedule']['name']);
				$this->set( 'notes', $schedule['Schedule']['notes']);
				$this->set( 'hash', $data['SchedulesPlayer']['hash'] );
				$this->set( 'schedule_id', $schedule_id );
				$this->set( 'date', $schedule['Schedule']['date'] );
				
				$this->Email->send( );
				$count++;
			}
		}

		return $count;
	}

}

?>
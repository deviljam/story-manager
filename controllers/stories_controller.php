<?php
class StoriesController extends AppController {

	function index () {
		//Show Story list
		$stories = $this->Story->find('all');
		
		$this->set( compact( 'stories' ) );
	}
	
	function add( ) {
		//Create a new story
		if ( !empty ( $this->data ) ) {
			$this->Story->create();
			if ( $this->Story->save( $this->data ) ) {
				$this->flashSuccess( false, array( 'action' => 'index' ) );
			} else { 
				//Did not save
				$this->Session->setFlash( "Failed to save" );
			}
		}
	}
	
	function edit( $id = null ) {
		//Create a new story
		if ( !empty ( $this->data ) ) {
			if ( $this->Story->save( $this->data ) ) {
				$this->flashSuccess( false, array( 'action' => 'index' ) );
			} else { 
				//Did not save
				$this->Session->setFlash( "Failed to save" );
			}
		} else { 
			$this->Story->id = $id;
			$this->data = $this->Story->read();
		}
	}
	
	function view( $id = null ) {
		$this->Story->id = $id;
		$story = $this->Story->read();
		
		$this->set( 'story_id', $id );
		$this->set( compact ( 'story' ) );
	}
} 
?>
<?php

class ChaptersController extends AppController {

	function view ( $id = null ) {
		$this->Chapter->id = $id;
		$chapter = $this->Chapter->read();
		
		$this->set( compact ( 'chapter' ) );
	}
	
	function edit ( $id = null ) {	
		$this->loadModel ( 'Prop' );
		$this->loadModel ( 'Story' );
		
		//Get current Chapter and story id
		$this->Chapter->id = $id;
		$current_chapter = $this->Chapter->read();
		$story_id = $current_chapter['Chapter']['story_id'];
	
		if ( !empty ( $this->data ) ) {
			$story_id = $this->data['Chapter']['story_id'];
		
			if ( $this->Chapter->save( $this->data ) ) {
				$this->flashSuccess ( false, "/stories/view/$story_id" );
			} else {
				$this->Session->setFlash( "Could not save" );
			}
		} else { 
			$this->data = $current_chapter;
		} 
		
		$story_options = $this->Story->find('list');
		$props_options = $this->Prop->find('list');
		$props_options[0] = '- No Prop -';
		
		
		$this->set ( compact ( array( 'props_options', 'story_options' ) ) );
	}
	
	function add ( $story_id = 0 ) {	
		$this->loadModel ( 'Prop' );
		$this->loadModel ( 'Story' );
		
		if ( !empty ( $this->data ) ) {
			$story_id = $this->data['Chapter']['story_id'];
			
			if ( $this->Chapter->save( $this->data ) ) {
				$this->flashSuccess ( false, "/stories/view/$story_id" );
			} else {
				$this->Session->setFlash( "Could not save" );
			}
		} else {
			$this->data['Chapter']['story_id']= $story_id;
		}
		
		$story_options = $this->Story->find('list');
		$props_options = $this->Prop->find('list');
		$props_options[0] = '- No Prop -';
		
		
		$this->set ( compact ( array( 'props_options', 'story_options' ) ) );
	}
	
	function complete ( $id = null) {
		$this->Chapter->id = $id;
		$current_chapter = $this->Chapter->read();
		
		if ( $current_chapter['Chapter']['complete'] != 1  ) {
			$complete = 1;
		} else { 
			$complete = 0;
		}
		
		$data = array( 'Chapter' => array ( 'id' => $id, 'complete' => $complete ) );
		if ( $this->Chapter->save( $data ) ) {
			$this->flashSuccess('complete', '/', array( 'id' => $id, 'complete' => $complete ) );
		}
		
		die ( "No save" );
	}

} 

?>
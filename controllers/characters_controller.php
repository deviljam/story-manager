<?php

class CharactersController extends AppController { 

	public $characterTemplate_vampire = array(
		array( "name" => "clan", "value" => "undecided"),
		array( "name" => "strenght", "value" => 1),
		array( "name" => "dexterity", "value" => 1),
		array( "name" => "stamina", "value" => 1),
		array( "name" => "charisma", "value" => 1),
		array( "name" => "manipulation", "value" => 1),
		array( "name" => "appearance", "value" => 1),
		array( "name" => "perception", "value" => 1),
		array( "name" => "intelligence", "value" => 1),
		array( "name" => "wits", "value" => 1),
		array( "name" => "alertness", "value" => 0),
		array( "name" => "athletics", "value" => 0),
		array( "name" => "brawl", "value" => 0),
		array( "name" => "dodge", "value" => 0),
		array( "name" => "empathy", "value" => 0),
		array( "name" => "expression", "value" => 0),
		array( "name" => "intimidation", "value" => 0),
		array( "name" => "leadership", "value" => 0),
		array( "name" => "streetwise", "value" => 0),
		array( "name" => "subterfuge", "value" => 0),
		array( "name" => "animal_ken", "value" => 0),
		array( "name" => "crafts", "value" => 0),
		array( "name" => "drive", "value" => 0),
		array( "name" => "etiquette", "value" => 0),
		array( "name" => "firearms", "value" => 0),
		array( "name" => "melee", "value" => 0),
		array( "name" => "performance", "value" => 0),
		array( "name" => "security", "value" => 0),
		array( "name" => "stealth", "value" => 0),
		array( "name" => "survival", "value" => 0),
		array( "name" => "acedemics", "value" => 0),
		array( "name" => "computer", "value" => 0),
		array( "name" => "finance", "value" => 0),
		array( "name" => "investigation", "value" => 0),
		array( "name" => "law", "value" => 0),
		array( "name" => "linguistics", "value" => 0),
		array( "name" => "medicine", "value" => 0),
		array( "name" => "occult", "value" => 0),
		array( "name" => "politics", "value" => 0),
		array( "name" => "science", "value" => 0)
	);
	
	public $characterTemplate_2d6 = array(
		array( "name" => "profession", "value" => "undecided"),
		array( "name" => "strength", "value" => -1),
		array( "name" => "fitness", "value" => -1),
		array( "name" => "balance", "value" => -1),
		array( "name" => "perception", "value" => -1),
		array( "name" => "intelligence", "value" => -1),
		array( "name" => "willpower", "value" => -1)
	);

	//Specifically for character sheets for specific people or groups
	function index ( ) {
		$characters = $this->Character->find('all', array( 'order' => 'name' ) );
		$this->set( "characters", $characters );
	}
	
	
	function view ( $id = null ) {
		$this->loadModel ( "Prop" );
		$discipline_list = $this->Prop->find('list', array ( 'conditions' => array ( 'category_id' => 4 ) ) );
		$equipment_list = $this->Prop->find('list', array ( 'conditions' => array ( 'category_id' => 5 ) ) );
		
		$this->Character->id = $id;
		$character = $this->Character->read();
		$stat_list = array();
		foreach( $character['CharacterStat'] as $stat ) {
			$stat_list[ $stat['name'] ] = $stat['value'];
		}
		$character['StatList'] = $stat_list;
		
		$this->set( "character", $character );
		$this->set( "discipline_list", $discipline_list );
		$this->set( "equipment_list", $equipment_list );	

		$type = $character['Character']['layout'];
		
		$this->render( "view_$type" );
	}
	
	function edit ( $id = null ) {
		
		if ( !empty ( $this->data ) ) {
			if ( $this->Character->save( $this->data ) ) {
				$out = ( 'Character saved' );
			} else {
				$out = ( 'Character did not save' );
			}
		} 
	
		echo json_encode( array (
			"message" => $out
		) ); 
		die;
	}
	
	function add ( $type="vampire") {
		//Create a character, then show the edit page
		$this->loadModel ( "CharacterStat" );
		
		$data = array ( "Character" => array( "name"=>"new", 'layout'=>$type ) );
		if ( $this->Character->save( $data ) ) {
			$id = $this->Character->getInsertID();
			
			switch( $type ){
				case "2d6" : $template = $this->characterTemplate_2d6; break;
				default: $template = $this->characterTemplate_vampire; break;
			}
			
			foreach ( $template as $t ){
				$t['character_id'] = $id;
				$this->CharacterStat->create();
				$this->CharacterStat->save( $t );
			}
			
			$this->redirect ( "/characters/view/$id" );
		}
		$this->Session->setFlash( "There was an error saving" );
		$this->redirect ( "/characters/" );
	}
	
	function xhr_newprop( $type, $character_id ) {
		$cat = $type == 5 ? 'CharacterEquipment' : 'CharacterDiscipline' ;
		
		$this->loadModel ( "Prop" );
		$this->loadModel ( $cat );
		
		//Create a new Discipline/Equipment
		$this->{$cat}->save( array( $cat => array( 'character_id' => $character_id ) ) );
		$id = $this->{$cat}->getInsertId();		
		
		$list = $this->Prop->find('list', array ( 'conditions' => array ( 'category_id' => $type ) ) );
		
		$options = "";
		$dots = "<input type='button' name='data[$cat][$id][level]' value='0' />";
	
		foreach ( $list as $key => $item ) {
			$options .= "<option value='$key'>$item</option>";
		}
	
		die ( "<select name='data[$cat][$id][prop_id]' >$options</select>$dots" );
	}

}
?>
<?php

class PropsController extends AppController {

	function beforeFilter() {
		parent::beforeFilter();
		$this->set ( 'categories', $this->Prop->Category->find('list') );
		$this->Auth->allow( 'index', 'view', 'map' );
	}
	
	function index( $category = false ) {
	
		$conditions = array();
		
		if ( !$this->Auth->user('id') ) {
			$conditions['Prop.public'] = 1;
		}
		if ( $category ) { 
			$conditions['Prop.category_id'] = $category;	
		} 
		
		$props = $this->Prop->find( 'all', array( 
			'conditions' => $conditions,
			'order' => 'Prop.category_id, Prop.name' ) 
		);	
		$this->set ( compact( 'props' ) );
	}
	
	function map() {
		$conditions = array ( 'not' => array ( 'lat' => 0, 'lng' => 0 ) );
		
		if ( !$this->Auth->user('id') ) {
			$conditions['Prop.public'] = 1;
		}
		
		$props = $this->Prop->find( 'all', array( 'conditions' => $conditions ) );	
		
		$this->set ( compact( 'props' ) );
	}
	
	function view ( $id_slug = null ) {
	
		if ( preg_match ( '/^[\d]+$/', $id_slug ) ) {		
			//Id match Slug
			$this->Prop->id = $id_slug;
			$prop = $this->Prop->read();
		} else {
			//Slug match
			$prop = $this->Prop->findSlug( $id_slug );
		}
		
		if ( !$this->Auth->user('id') ) {
			if ( $prop['Prop']['public'] <= 0 ) {
				$this->flashFailure( "You must be logged in to view " . $prop['Prop']['name'], "/props" );
			}
		}
		
		$this->set ( compact ( 'prop' ) );
	}
	
	function add ( $id = null ) {
		$this->loadModel ( 'Story' );
		
		if ( !empty( $this->data ) ) {
			if ( $this->Prop->save ( $this->data ) ) {
				$this->flashSuccess ( false, "/props" );
			} else {
				$this->Session->setFlash ( "Unable to save" );
			}
		}
		
		$story_options = $this->Story->find('list');
		$story_options[0] = '- No Story -';
		
		$this->set( 'story_options', $story_options );
	}
	
	function edit ( $id = null ) {
		$this->loadModel ( 'Story' );
		
		if ( !empty( $this->data ) ) {
			if ( $this->Prop->save ( $this->data ) ) {
				$this->flashSuccess ( false, "/props" );
			} else {
				$this->Session->setFlash ( "Unable to save" );
			}
		} else { 
			$this->Prop->id = $id;
			$this->data = $this->Prop->read();
		}
		
		$story_options = $this->Story->find('list');
		$story_options[0] = '- No Story -';
		
		$this->set( 'story_options', $story_options );
	}

	function delete ($id = null){
		$this->Prop->delete($id);

		$this->Session->setFlash( "Prop deleted" );

		$this->redirect('/props/');
	}
	
	function timeline() {
		$props = $this->Prop->timeline();
		$this->set ( 'props', $props );
	}
	
}
?>
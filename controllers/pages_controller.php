<?php

class PagesController extends AppController {
	var $name = 'Pages';
	var $helpers = array('Html', 'Session');
	var $uses = array();
	
	function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}
	
	function combat_2d6() {
		$this->loadModel('Damage');
		$damage = $this->Damage->find('all');
		$out = array();
		foreach($damage as $key => $dam){
			$type = $dam['Damage']['type'];
			$degree = $dam['Damage']['degree'];
			
			if ( !array_key_exists( $type, $out ) ) {
				$out[ $type ] = array(1=>array(),2=>array(),3=>array(),4=>array(),5=>array(),6=>array());
			}
			array_push( $out[ $type ][ $degree ], $dam['Damage'] );
		}
		
		$this->set('damage',$out);
	}
}

?>
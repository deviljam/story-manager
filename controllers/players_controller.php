<?php

class PlayersController extends AppController {
	
	function index () {
		$players = $this->Player->find( 'all' );
		$this->set ( "players", $players );
	}
	
	function edit ( $id = null ) {
		if ( $id == null ) {
			$this->redirect ( "index" );
		}
		if ( !empty( $this->data ) ) {
			if ( $this->Player->save() ){
				$this->flashSuccess( "Player saved", "/players" );
			} else {
				$this->flashFailure( "Player failed to save.");
			}
		} else { 
			$this->Player->id = $id;
			$this->data = $this->Player->read();
		}
	}
	
	function add ( ) {

		if ( !empty( $this->data ) ) {
			if ( $this->Player->save() ){
				$this->flashSuccess( "Player saved", "/players" );
			} else {
				$this->flashFailure( "Player failed to save.");
			}
		}
	}
	
	function delete ( $id = null ) {
		if ( $id == null ) {
			$this->redirect ( "index" );
		}
		if ( $this->Player->delete ( $id ) ) {
			$this->flashSuccess( "Player deleted", "/players" );
		}
		
		$this->flashFailure( "Unable to delete player", "/players" );
	}
	
}

?>
<?php

class ParagraphsController extends AppController {
	function index () {
		$paragraphs = $this->Paragraph->find('all');
		$articles = array();

		foreach ( $paragraphs as $p ) {
			preg_match_all('/#([a-zA-Z0-9_-]+)/', $p['Paragraph']['body'], $groups);
			foreach( $groups[1] as $match ){
				if ( isset ( $articles[ $match ]) ) {
					$articles[ $match ]['count']++;
				} else {
					$articles[ $match ] = array( 'count' => 1, 'name' => $match );
				}
			} 
		}
		uksort($articles, "listsort");
		$this->set( compact('articles'));
	} 

	function all () {
		$paragraphs = $this->Paragraph->find('all');
		
		$this->set ( compact('paragraphs') );
	}
	
	function json_filter( $filter = false ) {
		
		$paragraphs = $this->Paragraph->filter($filter);
		
		echo json_encode( $paragraphs );
		die;
		
	}
	
	function summary( $filter = false, $associates_level = 0 ) {
		$paragraphs = $this->Paragraph->filter($filter,$associates_level);
		
		$this->set ( compact ( 'paragraphs', 'filter' ) );
	}
	
	function edit ( $id = null ) {
	
		$this->Paragraph->id = $id;
		
		if ( !empty ( $this->data ) ) {
			if ( $this->Paragraph->save ( $this->data ) ) {
				echo json_encode ( $this->Paragraph->read() );
				die;
			}
		} else { 
			$this->data = $this->Paragraph->read();
		}
	}
	
	function add ( ) {
		
		if ( !empty ( $this->data ) ) {
			if ( $this->Paragraph->save ( $this->data ) ) {
				echo json_encode ( $this->Paragraph->read() );
				die;
			}
		}
	}
}

?>
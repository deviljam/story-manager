<?php

//For Sorting Props by their time
function timesort($a, $b ) {
	if ( $a['Time']['stamp'] == $b['Time']['stamp'] ) {
		return 0;
	}
	return 
		$a['Time']['stamp'] > $b['Time']['stamp'] ? 1 : -1;
}

function listsort($a, $b ) {
	if ( $a['name'] == $b['name'] ) {
		return 0;
	}
	return 
		$a['name'] > $b['name'] ? 1 : -1;
}


?>
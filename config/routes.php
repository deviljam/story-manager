<?php

	Router::connect('/', array('controller' => 'schedules', 'action' => 'index'));
	Router::connect('/ajax/*', array('controller' => 'pages', 'action' => 'home', 'home'));
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	
	Router::connect('/combat_2d6/*', array('controller' => 'pages', 'action' => 'combat_2d6'));

?>
//Script to appear on every page.

//Universal Onload function
var onload_buffer = new Array();

window.onload = function () {
	for( var i in onload_buffer ) {
		onload_buffer[i]();
	}
};

//Convience extension to get div element position
HTMLElement.prototype.getTopLeft = function() {
	var top = Math.floor( this.style.top.replace('px','') );
	var left = Math.floor( this.style.left.replace('px','') );
	return new Point(left,top);	
}

HTMLElement.prototype.hasClass = function( className ) {
	var classArray = this.className.split(' '); 
	for ( var i in classArray ) {
		if ( className == classArray[i] ) {
			return true;
		}
	}
	return false;
}

//Reusable objects
function Point(x,y) {
	this.x = x;
	this.y = y;	
}

function Rectangle(x,y,w,h) {
	this.x = x;
	this.y = y;	
	this.w = w;
	this.h = h;
	
	this.isWithin = function ( p ) {
		var out = 
			p.x >= this.x &&
			p.y >= this.y &&
			p.x < this.x + this.w &&
			p.y < this.y + this.h;	
		return out;
	}
}

//Reusable functions
function getMousePosition ( ev, sender ) {
	if ( typeof( ev ) != 'undefined' ) {
		return new Point(
			ev.clientX - sender.offsetLeft + window.scrollX,
			ev.clientY - sender.offsetTop + window.scrollY
		);
	} else {
		
	}
}

function showModal(markup){
	var modal = document.getElementById( 'modal' );

	if ( modal == undefined ){
		modal = document.createElement('div');
		modal.id = "modal";
		document.body.appendChild( modal );
	} else{
		modal.style.display = "block"
	}

	modal.innerHTML = "";
	modal.appendChild( markup );
}

function link_warning( self, message ){
	if ( message == undefined) {
		message = "Are you sure?"
	}

	var container = document.createElement( 'div' );
	var message_area = document.createElement( 'p' );
	var options = document.createElement( 'div' );
	var okay = document.createElement( 'a' );
	var cancel = document.createElement( 'a' );
	var clear = document.createElement( 'div' );

	clear.className = "clearfix"
	container.className = "modal_panel"
	message_area.innerHTML = message;
	okay.innerHTML = "Okay";
	cancel.innerHTML = "Cancel";
	options.appendChild( okay );
	options.appendChild( cancel );
	container.appendChild( message_area );
	container.appendChild( options );
	container.appendChild( clear );

	okay.onclick = function() {
		window.location = self.href;
	}
	cancel.onclick = function() {
		var modal = document.getElementById('modal');
		if( modal != undefined ){
			modal.style.display = "none";
		}
	}

	showModal( container );
	return false;
}
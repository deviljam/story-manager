function Paragraph( elem ) {
	
	// Functions
	this.set = function ( value ) {
		this.body = value.replace(/^[\s\t\r\n]+/, '').replace(/[\s\t\r\n]+$/, '');
		this.preview.innerHTML = this.parsePreview( this.body );
		this.textarea.innerHTML = this.body;
		
		
		this.preview.style.display = 'block';
		this.textarea.style.display = 'none';
		this.controls.style.display = 'none';
	}
	
	this.save = function () {
		this.body = this.textarea.value;
		
		$.ajax( {
			url : '/paragraphs/edit/' + this.id,
			type : 'POST',
			data : { data : { Paragraph : { body : this.body } } },
			success : function( response ) {
				res = JSON.parse( response ) ;
				$( '.paragraph[data-id=' + res.Paragraph.id + ']' ).each( 
					function() {
						this.paragraph.set( res.Paragraph.body );
					}
				);
			}
		} );
	}

	this.cancel = function(){
		this.preview.style.display = 'block';
		this.textarea.style.display = 'none';
		this.controls.style.display = 'none';

		this.textarea.value = this.body;
	}
	
	this.showEditor = function() {
		this.preview.style.display = 'none';
		this.textarea.style.display = 'block';
		this.controls.style.display = 'block';

		this.textarea.focus();
	}
	
	this.parsePreview = function( text ) {
		link = '<a href="#" rel="$1" onclick="return apply_hash( this.rel );">#$1</a>'
		text = text.replace( /#([a-zA-Z0-9_]+)/gi, link );
		return text;
	}
	
	// Init
	
	this.element = elem;
	this.id = $( elem ).data( 'id' );
	this.body = elem.innerHTML.replace(/^[\s\t\r\n]+/, '').replace(/[\s\t\r\n]+$/, '');
	
	this.preview = document.createElement( 'p' );
	this.preview.innerHTML = this.parsePreview( this.body );
	this.preview.onclick = function() { this.paragraph.showEditor(); return false; }
	
	this.textarea = document.createElement( 'textarea' );
	this.textarea.style.display = 'none';
	this.textarea.innerHTML = this.body

	this.savebutton = document.createElement( 'a' );
	this.savebutton.innerHTML = "Save";
	this.savebutton.onclick = function(){ this.paragraph.save(); return false;}
	
	this.cancelbutton = document.createElement( 'a' );
	this.cancelbutton.innerHTML = "Cancel";
	this.cancelbutton.onclick = function(){ this.paragraph.cancel(); return false;}

	this.savebutton.paragraph = 
	this.cancelbutton.paragraph = 
	this.textarea.paragraph = 
	this.preview.paragraph = 
	this.element.paragraph = this;

	this.controls = document.createElement( 'div' );
	this.controls.className = "controls";
	this.controls.style.display = "none";
	this.controls.appendChild( this.savebutton );
	this.controls.appendChild( this.cancelbutton );
	
	this.element.innerHTML = "";
	this.element.appendChild( this.preview );
	this.element.appendChild( this.textarea );
	this.element.appendChild( this.controls );
	
}

onload_buffer.push ( function () {
	$( '.paragraph' ).each ( function() {
		new Paragraph( this );
	} );
} );
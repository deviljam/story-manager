var Calendar = function( elem ) {

	//Function
	this.renderMonth = function( y, m ) {
		var year = this.getYear( y );
		var month = year.getMonth( m );
		
		this.calendar_element.innerHTML = "";	
		
		var new_nav = document.createElement( 'div' );
		var new_ul = document.createElement( 'ul' );
		
		var new_prev = document.createElement( 'a' );
		var new_month = document.createElement( 'h3' );
		var new_next = document.createElement( 'a' );
		
		new_month.innerText = this.monthsOfTheYear[ m ] + " " + this.year;
		new_nav.className = 'nav';
		
		new_prev.className = "calendar_prev";
		new_prev.onclick = function ( ev ) { 
			this.calendar.nextMonth( -1 );
			return false;
		};
		new_prev.innerText = "Previous";
		new_prev.calendar = this;
		
		new_next.className = "calendar_prev";
		new_next.onclick = function ( ev ) {
			this.calendar.nextMonth( 1 );
			return false;
		};
		new_next.innerText = "Next";
		new_next.calendar = this;
		
		new_nav.appendChild ( new_prev );
		new_nav.appendChild ( new_month );
		new_nav.appendChild ( new_next );
		
		
		for ( var i = 0; i < this.daysOfTheWeek.length; i++ ) {
			//Adding Day header
			var new_li = document.createElement( 'li' );
			new_li.className = 'header';
			new_li.innerText = this.daysOfTheWeek[ i ];
			
			new_ul.appendChild ( new_li );
		}
		
		for ( var i = 0; i < month.firstDay; i++ ) {
			//Adding filler
			var new_li = document.createElement( 'li' );
			new_li.className = 'blank';
			
			new_ul.appendChild ( new_li );
		}		
		
		for ( var i = 1; i <= month.count; i++ ) {
			//Adding days			
			var day = month.getDay(i);
			var date = day.date;
			
			var new_li = document.createElement( 'li' );
			var new_span = document.createElement( 'span' );
			
			new_span.className = "day";
			new_span.innerText = date.getDate();
			
			new_li.appendChild( new_span );
			for ( var j in day.items ) {
				day.items[j].style.display = 'block';
				new_li.appendChild ( day.items[j] );
			}
			
			new_ul.appendChild ( new_li );
		}
		
		this.calendar_element.appendChild( new_nav );
		this.calendar_element.appendChild( new_ul );
	}
	
	this.addItem = function ( date, item ) {
		var year = date.getYear() > 1900 ? date.getYear() : date.getYear() + 1900;
		var month = date.getMonth();
		var day = date.getDate();
		
		var current_year = this.getYear ( year );
		current_year.addItem( day, month, item );
	}
	
	this.getYear = function( year ) {
		var current_year = false;
		for ( var i in this.years ) {
			if ( this.years[i].year == year ) {
				current_year = this.years[i]; 
			}
		}
		
		if ( current_year == false ) {
			current_year = new CalendarYear( year )
			this.years.push( current_year );
		}
		
		return current_year;
	}
	
	this.nextMonth = function ( count ) {
		var new_month = this.month + count;
		var year_dif = Math.floor ( new_month / 12 );
		
		this.year += year_dif;
		this.month = ( new_month + 12 ) % 12;
		
		this.renderMonth( this.year, this.month );
	}
	
	//Init
	var now = new Date();

	this.element = elem;
	this.element.calendar = this;
	
	this.years = [];
	
	this.year = now.getYear() > 1900 ? now.getYear() : now.getYear() + 1900;;
	this.month = now.getMonth();
	this.day = now.getDay();
	
	this.years.push ( new CalendarYear( this.year ) );
	
	this.daysOfTheWeek = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
	this.monthsOfTheYear = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];
	
	//Transform all divs into events 
	for ( var i in this.element.childNodes ) {
		if ( this.element.childNodes[i].tagName ) {
			//Get current element's title
			var title = this.element.childNodes[i].title;
			//Get Time Stamp
			if ( title.match( /\d\d\d\d-[0-1]\d-[0-3]\d/ ) ) {
				var split_date = title.split( '-' );
				var date = new Date ( split_date[0], split_date[1] - 1, split_date[2], 0, 0, 0, 0 );
				//var date = new Date ( title );
				this.addItem ( date, this.element.childNodes[i] );
			}
			//Hide all existing elements
			this.element.childNodes[i].style.display = 'none';
		}
	}
	
	this.calendar_element = document.createElement( 'div' );
	this.element.appendChild( this.calendar_element );	
	
	this.renderMonth( this.year, this.month );
}

var CalendarDay = function ( day, month, year ) {
	
	this.day = day;
	this.month = month;
	this.year = year;
	
	this.date = new Date(this.year, this.month, this.day, 0, 0, 0, 0 );
	
	this.items = [];
}

var CalendarMonth = function ( month, year ) {
	
	this.getDay = function( i ) {
		return this.days[ i - 1 ];
	}
	
	this.addItem = function( d, item ) {
		var day = this.getDay ( d );
				
		//Push node into item array
		day.items.push( item );
	}

	this.year = year;
	this.month = month;
	this.count = 31;
	this.days = [];
	
	if ( month == 8 || month == 3 || month == 5 || month == 10 ) {
		this.count = 30;
	}
	
	if ( month == 1 ) {
		if ( year % 400 == 0 || ( year % 4 == 0 && year % 100 != 0 ) ) {
			this.count = 29;
		} else { 
			this.count = 28;
		}
	}
	
	for ( var i = 1; i <= this.count; i++ ) {
		this.days.push ( new CalendarDay( i, month, year ) );
	}
	
	this.firstDate = this.getDay( 1 ).date;
	this.firstDay = this.firstDate.getDay();
}

var CalendarYear = function ( year ) {
	
	this.getMonth = function ( i ) {
		return this.months[ i ];
	}
	
	this.addItem = function( d, m, item ) {
		var month = this.getMonth ( m );
		month.addItem( d, item );
	}
	
	this.year = year;
	this.leapYear = false;
	this.count = 12;
	this.months = [];
	
	for ( var i = 0; i < this.count; i++ ) {
		this.months.push( new CalendarMonth( i, this.year ) );
	}
	
	this.firstDate = this.getMonth( 0 ).firstDate;
	this.firstDay = this.firstDate.getDay();
}


onload_buffer.push( function() {
	new Calendar( document.getElementById( 'calendar' ) );

} );
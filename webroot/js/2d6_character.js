function ScoreButton( elem ){
	this.init = function( elem ){
		this.element = elem;
		this.element.score = this;
		this.element.onmousemove = function( e ) {
			
		}
		this.element.onclick = function( e ) {
			this.value = this.score.xToValue( e.offsetX );
			this.className = "value_" + this.value;
		}
		
		this.element.className = "value_" + this.element.value;
	}
	
	this.xToValue = function( x ) {
		return Math.floor( x / 14 ) - 2;
	}
	
	this.init( elem );
	return this;
}

$( '#add_new_skill, #add_new_talent' ).click( function( ) {
	var type = $(this).data('type');
	var name = $('#new_' + type).val();
	
	if ( true ) {
		var fullname = type + "_" + name;
		var elm_li = document.createElement( 'li' );
		var elm_label = document.createElement( 'label' );
		var elm_input = document.createElement( 'input' );
		
		elm_label.innerHTML = name;
		elm_input.type = "button";
		elm_input.name = "data[Stat]["+ fullname +"]";
		elm_input.value = "1";
		new ScoreButton( elm_input );
		
		elm_li.appendChild( elm_label );
		elm_li.appendChild( elm_input );
		
		$(this).parent().parent().prepend( elm_li );
	}
} );

$('#save_character').click( function() { 
	var data = {};
	var url = this.href;
	$(".character_sheet_2d6 input").each( function( item ) {
		data[ this.name ] = this.value;
	} );
	$.ajax({
		url : url,
		type : "POST",
		data : data,
		success : function( res ) {	alert ( "Save complete" ); }
	})
	return false;
} );

$('input[type=button]').each( function() {
	new ScoreButton( this );
} );
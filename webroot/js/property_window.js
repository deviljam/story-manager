// JavaScript Document

function showPropertyWindow( url, params ) {
	var prop = document.getElementById( 'properties' );
	var data = document.getElementById( 'properties_data' );
	var layout = 'layout:ajax';
	
	//Clear current data
	data.innerHTML = '';
	
	if ( url.indexOf ( layout ) < 0 ) {
		url += "/" + layout;
	}
	
	if ( prop ) {
		prop.style.display = 'block';
	} else {
		return false;	
	}
	
	//Make request
	$.ajax ( {
		url : url,
		success : function ( response ) {
			var prop = document.getElementById( 'properties' );
			var data = document.getElementById( 'properties_data' );
			if ( prop ) {
				data.innerHTML = response;	
				repointSlugLinks();
			}
		},
		error : function () {
			var prop = document.getElementById( 'properties' );
			if ( prop ) {
				prop.style.display = 'none';
			} 
		}
	});
}

function slugClicked( ev ) {
	showPropertyWindow ( this.href );
	ev.preventDefault();
}

function repointSlugLinks() {
	//Go through all slugs and point them to ShowPropertyWindow
	
	var links = document.getElementsByTagName( 'a' );
	
	for ( var i in links ) {
		if ( links[i].hasClass && links[i].hasClass( 'slug' ) ) {
			links[i].onclick = slugClicked;
		}
	}
}

onload_buffer.push ( function() {
	//Call from onload event
	var prop_close = document.getElementById( 'properties_close' );
	
	prop_close.onclick = function ( ev ) {
		var prop = document.getElementById( 'properties' );
		prop.style.display = 'none';
		ev.preventDefault();
	}
	
	repointSlugLinks();
	
} );
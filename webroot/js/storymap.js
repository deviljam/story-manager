// JavaScript Document

function StoryMap( elem ) {
	this.element = elem;
	this.element.storyMap = this;
	this.chapters = [];
	
	for ( var i in elem.childNodes ) {
		if ( elem.childNodes[i].hasClass && elem.childNodes[i].hasClass( "chapter" ) ) {
			this.chapters.push ( new Chapter( elem.childNodes[i] ) );
		}
	}
	
	this.dragging = false;
	
	//Functions 
	
	//Add listeners
	this.element.onmousemove = function( ev ) {
		var mouse = getMousePosition( ev, this );
		
		if ( this.storyMap.dragging ) {
			this.storyMap.dragging.setPosition ( mouse );
		}
	};
	
	this.element.onmousedown = function( ev ) {
		var chapter_drag = false;
		var mouse = getMousePosition( ev, this );
		
		for ( var i in this.storyMap.chapters ) {
			if ( this.storyMap.chapters[i].overDragarea( mouse ) ) {
				this.storyMap.dragging = this.storyMap.chapters[i];
				break;
			}
		}
		
		ev.preventDefault();	
	}
	
	this.element.onmouseup = function( ev ) {
		if ( this.storyMap.dragging ) {
			//Use AJAX to update the position
			var chapter_id = this.storyMap.dragging.id;
			
			$.ajax ( {
				url : '/chapters/edit/'+ chapter_id +'/layout:ajax',
				type: 'POST',
				data : {
					'data[Chapter][pos_x]' : this.storyMap.dragging.position.x,
					'data[Chapter][pos_y]' : this.storyMap.dragging.position.y
				}
			} );
		}
		
		//Release Dragging Chapter
		this.storyMap.dragging = false;
		ev.preventDefault();	
	}
}


function Chapter( elem ) {
	this.element = elem;
	this.element.chapter = this;
	this.dragarea = false;
	this.complete = false;
	this.position = elem.getTopLeft();
	this.more = false;
	this.id = Math.floor( this.element.id.replace(/[^0-9]*/, '') );
	
	this._mouseoffset = new Point(0,0);
	
	for ( var i in elem.childNodes ) {
		if ( elem.childNodes[i].className == "dragarea" ) {
			this.dragarea = elem.childNodes[i];
		}
		if ( elem.childNodes[i].className == "complete" ) {
			this.complete = elem.childNodes[i];
		}
		if ( elem.childNodes[i].className == "more" ) {
			this.more = elem.childNodes[i];
		}
	}
	//Functions
	this.setPosition = function ( point ) {
		this.position.x = point.x + this._mouseoffset.x;
		this.position.y = point.y + this._mouseoffset.y;
		
		this.element.style.left = this.position.x + "px";			
		this.element.style.top = this.position.y + "px";
	}
	
	this.overDragarea = function( point ) {
		var rect = new Rectangle ( 
			this.position.x,
			this.position.y,
			160, 25 
		);
		this._mouseoffset.x = this.position.x - point.x;
		this._mouseoffset.y = this.position.y - point.y;

		var out = rect.isWithin( point );
		return out;
	}
	
	this.more.onclick = function ( ev ) {
		showPropertyWindow ( this.href );
		ev.preventDefault();
	}
	
	this.complete.onclick = function ( ev ) {
		$.ajax( { 
			url : this.href + "/layout:ajax",
			success: function ( res ) {
				res = JSON.parse( res );
				var id = res.id;
				var chapter = document.getElementById ( "chapter_" + id );
				
				if ( res.complete > 0 ) {
					chapter.className = 'chapter complete';
				} else {
					chapter.className = 'chapter';
				}
			}
		} );
		ev.preventDefault();
	}
}

onload_buffer.push ( function() {

	var story = new StoryMap( document.getElementById( 'storymap' ) );

} );
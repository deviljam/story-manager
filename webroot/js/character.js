// JavaScript Document

function bindSingleCharacter( elm ) {
		
	//Attach function to dots
	var dots = elm.getElementsByTagName( 'input' );
	var save = elm.getElementsByTagName( 'a' );
	
	for ( var i in save ) {
		if ( !save[i].onclick ) {
			if ( save[i].className == 'save_character' ) {
				save[i].character_sheet = elm;
				save[i].onclick = function( ev ) {
					var dots = this.character_sheet.getElementsByTagName( 'input' );
					var selects = this.character_sheet.getElementsByTagName( 'select' );
					var params = {};
					for ( var i in dots ) {
						params[ dots[i].name ] = dots[i].value;
					}
					for ( var i in selects ) {
						params[ selects[i].name ] = selects[i].value;
					}
					$.ajax( {
						url : this.href + "/layout:ajax",
						type : 'POST',
						data : params,
						success : function ( res ) {
							alert( 'Save complete' );
						}
					} );
					ev.preventDefault();	
				}
			} else if ( save[i].className == 'addprop' ) {
				save[i].character_sheet = elm;
				save[i].onclick = function( ev ) {
					$.ajax( {
						url : this.href,
						success : function( res ) {
							var parent_list = this.calledby.parentElement.parentElement;
							var li = document.createElement( "li" );
							li.innerHTML = res;
							
							parent_list.appendChild( li );
							bindCharacters();
						},
						calledby : this
					} );
					ev.preventDefault();
				};
			}
		}
	}
	
	for ( var i in dots ) {
		if ( dots[i].type == "button" ) {
			if ( ! dots[i].onclick ) {
				//Preview 
				dots[i].onmousemove = function( ev ) {
					mouse = getMousePosition( ev, this );
					var index = Math.floor( mouse.x / 12 );
					
					this.className = "value_" + index;
				}
				//Undo Preview
				dots[i].onmouseout = function( ev ) {
					mouse = getMousePosition( ev, this );
					var index = Math.max( Math.min( this.value, 8 ), 0 );
					
					this.className = "value_" + this.value;
				}
				//Set value
				dots[i].onclick = function( ev ) {
					mouse = getMousePosition( ev, this );
					var index = Math.floor( mouse.x / 12 );
					
					this.value = index;
					this.className = "value_" + index;
				}
				
				dots[i].className = "value_" + dots[i].value;
			}
		}
	}
}

function bindCharacters ( ) {
	//search markup for character sheets and apply object to them
	var elms = document.getElementsByTagName( 'article' );
	for	( var i in elms ) {
		if ( elms[i].className == 'character_sheet' ) {
			bindSingleCharacter ( elms[i] );
		}
	}
}

onload_buffer.push ( function() {

	bindCharacters();

} );
<?php 

class AppController extends Controller {

	var $components = array ( 'Session', 'Auth', 'Email' );
	
	function beforeFilter() {
		//Add custom helpers 
		$this->helpers[] = 'Slugger';
	
		if ( $this->isAjax() ) {
			$this->layout = "ajax";
		}
		
		$this->set( "loggedin", $this->Auth->user('id') );
	}
	
	function isAjax() {
		foreach ( $this->params['named'] as $key=>$name ) {
			if ( $key == 'layout' && $name == 'ajax' ) { 
				return true;
			}
		}
		return false;	
	}

	function flashSuccess ( $message = false, $redirect = false, $params = array() ) {
		if ( $this->isAjax() ) {
			//Echo simple JSON response
			$params['success'] = true;
			$params['message'] = $message;
			echo json_encode( $params );
			die;			
		} else { 
			//Set Flash and redirect
			if ( $message ) {
				$this->Session->setFlash ( $message );
			}
			if ( $redirect ) {
				$this->redirect ( $redirect );
			}
		}
	}
	
	function flashFailure ( $message = false, $redirect = false, $params = array() ) {
		if ( $this->isAjax() ) {
			//Echo simple JSON response
			$params['success'] = false;
			$params['message'] = $message;
			echo json_encode( $params );
			die;			
		} else { 
			//Set Flash and redirect
			if ( $message ) {
				$this->Session->setFlash ( $message );
			}
			if ( $redirect ) {
				$this->redirect ( $redirect );
			}
		}
	}
	
}
//End of file
?>
